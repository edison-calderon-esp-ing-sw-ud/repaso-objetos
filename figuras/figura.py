
class Figura:
    def __init__(self, p1, p2):
        # El Punto forma parte de Figura, es obligatorio y debe recibir 2 puntos (Relación de Composición)
        self.punto1 = p1
        self.punto2 = p2
        self.area = 0
        self.perimetro = 0
        self.nombre = 'Figura'

    def mostrar_area(self):
        print(f'El área del {self.nombre} es {self.area}')

    def mostrar_perimetro(self):
        print(f'El perímetro del {self.nombre} es {self.perimetro}')