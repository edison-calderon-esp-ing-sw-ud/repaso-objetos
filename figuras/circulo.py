from figura import Figura
from math import pi

class Circulo (Figura):   
    def __init__(self, p1, p2):
        Figura.__init__(self, p1, p2)
        self.nombre = 'Circulo'

    def calcular_area(self):
        radio = self.punto1.calcular_distancia(self.punto2)
        self.area = pi * (radio ** 2)

    def calcular_perimetro(self):
        radio = self.punto1.calcular_distancia(self.punto2)
        self.perimetro = 2 * pi * radio