from figura import Figura

class Cuadrado (Figura):
    def __init__(self, p1, p2):
        Figura.__init__(self, p1, p2)
        self.nombre = 'Cuadrado'
    
    def calcular_area(self):
        lado = self.punto1.calcular_distancia(self.punto2)
        self.area = lado ** 2

    def calcular_perimetro(self):
        lado = self.punto1.calcular_distancia(self.punto2)
        self.perimetro = lado * 4